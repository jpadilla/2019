---
duration: 25
presentation_url:
room:
slot:
speakers:
- Michael Lynch
title: "Why Good Developers Write Bad Tests"
type: talk
video_url:
---
Most developers still approach tests with the same old techniques they use
for production code. But test code is not like other code. To write it well,
we need to reexamine the principles and goals that define our best
practices.

In this talk, I'll discuss:

* What separates good tests from bad ones
* Why you should think twice before refactoring test functions
* How to identify anti-patterns in tests that hide bugs
* Why your test names are probably too short
* Why you should embrace magic numbers
