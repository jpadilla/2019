---
duration: 25
presentation_url:
room:
slot:
speakers:
- Ethan Cowan
title: "Ethics \u0026 Bias Detection in the Real World"
type: talk
video_url:
---
As algorithmic decision makers become more pervasive in day-to-day life,
there is an increased urgency to address inherent biases and their potential
for perpetuating societal inequities.  We will outline new and existing
methods for determining implicit bias in machine learning models and rating
systems.
