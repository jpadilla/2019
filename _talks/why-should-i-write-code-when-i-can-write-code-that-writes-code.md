---
duration: 40
presentation_url:
room:
slot:
speakers:
- James Powell
title: "Why should I write code when I can write code that writes code?"
type: talk
video_url:
---
The temptation to employ code-generating techniques in Python is strong.
Much of what is called "metaprogramming" in Python refers to the various
techniques through which we can write higher-order code: code that then
generates the code that solves our problem. This talk discusses various
common approaches for code-generation, their applicability to solving real
problems, and the reality of using these techniques in your work.
