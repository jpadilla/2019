---
name: Jorn Mossel
talks:
- "The Cat\u0027s Alive! Add Some Weirdness To Your Code With Quantum Computing"
---
Jorn obtained his Ph.D. from the University of Amsterdam in the field of
Theoretical Physics, during which he published 8 peer-reviewed papers on
quantum many-body systems out of equilibrium. Jorn then moved to London to
join Goldman Sachs to work on systematic investment strategies. Jorn
currently works as an equity risk researcher for Balyasny Asset Management
in NYC.
