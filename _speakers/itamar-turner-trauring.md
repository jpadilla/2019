---
name: Itamar Turner-Trauring
talks:
- "Beyond cProfile: performance optimization with sampling profilers and logging"
---
Itamar helps teams using Python ship faster code. He has been using Python
since 1999, and worked on scientific computing, distributed systems, and
more. His open source work includes Eliot, the causal logging library, and
in the past he was a contributed to Twisted. You can learn more about Python
performance at https://pythonspeed.com.
