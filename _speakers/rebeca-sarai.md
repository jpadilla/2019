---
name: Rebeca Sarai
talks:
- "Jane Doe will help you improve your project"
---
I'm a full stack developer at Vinta Software, working with Python and
Javascript to bring customers vision to life. I'm passionate about the
Python community, that's why I'm always involved with my local Django Girls
and PyLadies community. I listen to a huge amount of podcasts and love to
discuss politics and football. In my spare time, I like to study machine
learning and ride my motorcycle.
