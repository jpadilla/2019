---
name: Ryan Micallef
talks:
- "Less Talk, More Rock: Transfer Learning with Natural Language Processing"
---
Ryan Micallef is a research engineer at Cloudera Fast Forward Labs. He
researches emerging machine learning technologies and helps clients apply
them. Ryan is also an attorney barred in New York. He was an intellectual
property litigator focused on technical cases for almost a decade before
joining Fast Forward Labs. Ryan has a bachelor's degree in Computer Science
from Georgia Tech and a Juris Doctor degree from Brooklyn Law School. Ryan
spends his free time soldering circuits and wrenching motorcycles. He
teaches microcontroller programming at his local hackerspace, NYC Resistor.
