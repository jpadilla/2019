---
name: Jacob Deppen
talks:
- "Sprints - Let\u2019s do more of those!"
---
Jacob is a PhD candidate in archaeology at the University of Washington who
specializes in using and building data science tools for archaeological
research. The main product of his PhD research is `prospect`, a Python
package for simulating archaeological field projects. He is also the co-
founder and lead maintainer of [`pandas-
vet`](https://github.com/deppen8/pandas-vet), a `flake8` plugin for linting
`pandas` code, and the creator of [pandas in black and
white](https://deppen8.github.io/pandas-bw/), a set of opinionated `pandas`
flashcards. In addition to his efforts in front of a keyboard, he has
extensive archaeological field experience; he has worked many years in Ohio
and, for his PhD work, the Spanish island of Mallorca.
