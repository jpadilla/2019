---
name: Abid H. Mujtaba
talks:
- "Memoizing recursive functions: How decorators are more elegant than I imagined"
---
Abid H. Mujtaba is an Electrical Engineer and Theoretical Physicist by
training and a Software Developer by inclination. He is currently a part of
the Bloomberg Engineering Training and Documentation team and is responsible
for curriculum development and training newly hired Financial Software
Developers.
