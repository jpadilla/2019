---
name: Sarah Schattschneider
talks:
- "Airflow in Practice: How I Learned to Stop Worrying and Love DAGs"
---
Software Engineer at Blue Apron on the Data Engineering team. Work daily
using Python on our data pipeline. Excited by how Python is transforming
Data Engineering.
